package com.software.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-18 14:52
 * @description:
 **/
@Aspect
@Component
public class LogAspect {

    @Pointcut("@annotation(com.software.aop.annotation.LogRecord)")
    public void pointLog() {
    }

    @Before("pointLog()")  //前置通知
    public void pointLogBefore(JoinPoint point) {
        System.out.println("打印日志,前置信息。");
    }
    @After("pointLog()") //后置通知
    public void pointLogAfter(JoinPoint point) {
        System.out.println("打印日志,后置信息。");
    }

    @Around("pointLog()") //环绕通知
    public Object pointLogAround(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println("打印日志,环绕前置信息!");
        Object proceed = null;
        try {
            proceed = proceedingJoinPoint.proceed();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        System.out.println("打印日志,环绕后置信息!");
        return proceed;
    }
}
/*
 * 前置通知（@Before）：在目标方法调用之前调用通知
 *
 * 后置通知（@After）：在目标方法完成之后调用通知
 *
 * 环绕通知（@Around）：在被通知的方法调用之前和调用之后执行自定义的方法
 *
 * 返回通知（@AfterReturning）：在目标方法成功执行之后调用通知
 *
 * 异常通知（@AfterThrowing）：在目标方法抛出异常之后调用通知
 */