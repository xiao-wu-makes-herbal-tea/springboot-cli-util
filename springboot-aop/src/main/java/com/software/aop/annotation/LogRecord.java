package com.software.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-18 14:47
 * @description:
 **/
@Target(ElementType.METHOD)   //定义注解的使用范围为方法
@Retention(RetentionPolicy.RUNTIME )
public @interface LogRecord {
}

/*
 * 1.@Target ：注解的作用目标
 * Target(ElementType.TYPE)         作用在接口，类，枚举，注解（如@restcontroller）
 * Target(ElementType.FIELD)        作用于字段，枚举的常量 @filed
 * @(ElementType.METHOD)            作用于方法，@reqestmapping
 * Target(ElementType.CONSTRUCTOR)  作用于构造函数
 * Target(ElementType.PARAMETER)    作用于方法参数 @param
 */
/*
 * 2.@Retention 注解保留生命周期
 * source	   注解只保留在源文件，当Java文件编译成class文件的时候，注解被遗弃；被编译器忽略
 * class	   注解被保留到class文件，但jvm加载class文件时候被遗弃，这是默认的生命周期
 * runtime	   注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在
 */