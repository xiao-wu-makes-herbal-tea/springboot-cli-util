package com.software.aop.controller;

import com.software.aop.annotation.LogRecord;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-18 14:58
 * @description:
 **/
@RestController
@RequestMapping("/aop")
public class AopController {
    @LogRecord
    @RequestMapping("/hello")
    public String helloWorld(){
        return "Hello Aop!";
    }
}
