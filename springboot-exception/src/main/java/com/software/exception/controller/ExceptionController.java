package com.software.exception.controller;

import com.software.exception.exception.biz.ServiceException;
import com.software.exception.exception.enums.ExceptionCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-23 09:38
 * @description:
 **/
@RestController
public class ExceptionController {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);

    @RequestMapping("/exception")
    public String exceptionMethod() {
        logger.info("exception");
        throw new ServiceException("业务异常");
    }

    @GetMapping("/exception1")
    public boolean exception1(@RequestParam String name) {
        //如果姓名为空就手动抛出一个自定义的异常！
        logger.info("exception1==>:{}", name);
        if (name == null || name == "") {
            throw new ServiceException("姓名不能为空");
        }
        return true;
    }

    @GetMapping("/exception2")
    public boolean exception2(String name, Date startTime) {
        logger.info("exception2==>:{} -- {}", name, startTime);
        //抛出一个异常
        throw new ServiceException(ExceptionCode.NULL_POINTER_EXCEPTION);
    }
}

