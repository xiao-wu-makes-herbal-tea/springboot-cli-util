package com.software.exception.result;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-26 15:58
 * @description:
 **/
@Getter
@AllArgsConstructor
public enum ResultCode implements IResultCode {
    SUCCESS(200, "操作成功"),
    FAILURE(400, "操作失败");
    /**
     * 响应状态码
     */
    private final int code;
    /**
     * 响应消息体
     */
    private final String message;

}
