package com.software.exception.result;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-26 15:56
 * @description:
 **/
public interface IResultCode {
    /**
     * 获取状态码
     *
     * @return
     */
    int getCode();

    /**
     * 获取消息
     *
     * @return
     */
    String getMessage();
}
