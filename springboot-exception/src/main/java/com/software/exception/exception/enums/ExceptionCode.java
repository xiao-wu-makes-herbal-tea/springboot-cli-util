package com.software.exception.exception.enums;

import com.software.exception.result.IResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-26 16:58
 * @description:
 **/
@Getter
@AllArgsConstructor
public enum ExceptionCode implements IResultCode {

    /**
     * 自定义-> 400,4000--4999
     */
    SERVICE_EXCEPTION(400, "业务异常"),
    SIGNATURE_NOT_MATCH(400, "请求的数字签名不匹配!"),
    UNAUTHORIZED(401, "未授权!"),
    NOT_FOUND(404, "Not Found"),
    METHOD_NOT_ALLOWED(4005, "不支持当前请求方法"),
    MISSING_REQUEST_PARAM(4006, "缺少请求参数"),
    REQUEST_PARAMETER_FORMAT_INCORRECT(4007, "请求参数格式错误"),
    PARAMETER_VALIDATION_FAILURE(4008, "参数验证失败"),
    PARAMETER_BINDING_FAILURE(4009, "参数绑定失败"),
    MEDIA_TYPE_NOT_SUPPORTED(4010, "不支持当前媒体类型"),
    MESSAGE_UNREADABLE(4011, "消息不可读"),
    /**
     * 自定义-> 500,5000--5999
     */
    INTERNAL_SERVER_ERROR(500, "服务器内部错误"),
    SYSTEM_COMPUTATION_EXCEPTION(5001, "内部数据计算异常"),
    INTERNAL_DATA_FORMAT_ERROR(5002, "内部数据格式转换异常"),
    NULL_POINTER_EXCEPTION(5003, "空指针异常"),
    SERVER_BUSY(5004, "服务器正忙，请稍后再试!");
    /**
     * 响应状态码
     */
    private final int code;
    /**
     * 响应消息体
     */
    private final String message;
}
