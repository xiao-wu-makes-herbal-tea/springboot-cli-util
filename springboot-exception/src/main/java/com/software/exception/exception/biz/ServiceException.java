package com.software.exception.exception.biz;

import com.software.exception.exception.enums.ExceptionCode;
import com.software.exception.result.IResultCode;
import lombok.Getter;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-26 17:08
 * @description: 业务异常处理
 **/
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 2359767895161832954L;
    @Getter
    private final IResultCode resultCode;

    public ServiceException(String message) {
        super(message);
        this.resultCode = ExceptionCode.SERVICE_EXCEPTION;
    }

    public ServiceException(IResultCode resultCode) {
        super(resultCode.getMessage());
        this.resultCode = resultCode;
    }

    public ServiceException(IResultCode resultCode, Throwable cause) {
        super(cause);
        this.resultCode = resultCode;
    }

    /**
     * 提高性能
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    public Throwable doFillInStackTrace() {
        return super.fillInStackTrace();
    }
}
