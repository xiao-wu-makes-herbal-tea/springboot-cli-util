package com.software.helloworld.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-18 14:12
 * @description:
 **/
@RestController
@RequestMapping("/helloWorld")
public class HelloWorldController {
    @RequestMapping("/hello")
    public String helloWorld(){
        return "Hello World!";
    }
}
