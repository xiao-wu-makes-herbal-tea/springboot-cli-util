package com.software.mybatis.controller;

import com.software.mybatis.domain.User;
import com.software.mybatis.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-19 09:54
 * @description:
 **/
@RestController
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * 查询所有用户信息
     */
    @GetMapping("/user")
    public List<User> getUsers() {
        List<User> userList = userService.getUserList();
        return userList;
    }

    /**
     * 查询单个用户信息
     */
    @GetMapping("/user/{id}")
    public User getUser(@PathVariable int id) {
        User user = userService.getUserById(id);
        return user;
    }

    /**
     * 修改用户信息
     */
    @PostMapping("/user")
    public int updateById(@RequestBody User user) {
        return userService.updateById(user);
    }

    /**
     * 查询所有用户信息
     */
    @DeleteMapping("/user/{id}")
    public int updateById(@PathVariable int id) {
        return userService.deleteById(id);
    }

}
