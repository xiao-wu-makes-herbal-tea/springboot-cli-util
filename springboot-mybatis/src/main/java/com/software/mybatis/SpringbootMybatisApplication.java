package com.software.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMybatisApplication.class, args);
        System.out.println("mybatis: "+"http://localhost:9999/user");
        System.out.println("mybatis: "+"http://localhost:9999/user/{id}");
    }

}
