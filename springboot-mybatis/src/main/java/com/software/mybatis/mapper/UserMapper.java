package com.software.mybatis.mapper;

import com.software.mybatis.domain.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-19 09:54
 * @description:
 **/
@Mapper
public interface UserMapper {
    /**
     *	查询所有用户信息
     */
    List<User> selectAllUser();
    /**
     *	根据id查询用户信息
     */
    User selectOne(int id);
    /**
     *	根据id修改用户信息
     */
    @Update("update user set username=#{username},age=#{age} where id=#{id}")
    int updateById(User user);
    /**
     *	根据id删除用户信息
     */
    @Delete("delete from user where id=#{id}")
    int deleteById(int id);
}
