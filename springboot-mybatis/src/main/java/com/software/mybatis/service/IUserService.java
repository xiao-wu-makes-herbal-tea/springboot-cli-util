package com.software.mybatis.service;

import com.software.mybatis.domain.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-19 09:52
 * @description:
 **/
public interface IUserService {
    /**
     *	查询所有用户信息
     */
    List<User> getUserList();
    /**
     *	根据id查询用户信息
     */
    User getUserById(int id);
    /**
     *	根据id修改用户信息
     */
    int updateById(User user);
    /**
     *	根据id删除用户信息
     */
    int deleteById(int id);
}
