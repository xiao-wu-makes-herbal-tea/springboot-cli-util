package com.software.mybatis.service.impl;

import com.software.mybatis.domain.User;
import com.software.mybatis.mapper.UserMapper;
import com.software.mybatis.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-19 09:53
 * @description:
 **/
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getUserList() {
        return userMapper.selectAllUser();
    }

    @Override
    public User getUserById(int id) {
        return userMapper.selectOne(id);
    }

    @Override
    public int updateById(User user) {
        return userMapper.updateById(user);
    }

    @Override
    public int deleteById(int id) {
        return userMapper.deleteById(id);
    }
}
