package com.software.springbootpay.wxPay.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-19 15:47
 * @description:
 **/
@Data
@Accessors(chain = true)
public class NativePayParam {
    /*    M 为必填,C为可选   */
    /**
     * 公众号ID M
     */
    private String appid;
    /**
     * 直连商户号 M
     */
    private String mchid;
    /**
     *  商品描述 M
     */
    private String description;
    /**
     * 商户系统内部订单号 M
     */
    private String out_trade_no;
    /**
     * 订单金额信息 M
     */
    private Amount amount;
    /**
     * 支付者信息 M
     */
    private Payer payer;
    /**
     * 通知地址 M
     */
    private String notify_url;
    /**
     * 交易结束时间 C
     */
    private Date time_expire;
    /**
     * 附加数据 C
     */
    private String attach;
    /**
     * 订单优惠标记 C
     */
    private String goods_tag;
    /**
     * 电子发票入口开放标识 C
     */
    private boolean support_fapiao;
    /**
     * 订单详情 C
     */
//    private Detail detail;
    /**
     * 支付场景描述 C
     */
//    private Scene_info scene_info;
    /**
     * 结算信息 C
     */
//    private Settle_info settle_info;
}
