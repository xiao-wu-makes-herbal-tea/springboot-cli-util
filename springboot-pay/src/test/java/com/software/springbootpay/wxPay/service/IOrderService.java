package com.software.springbootpay.wxPay.service;

import com.software.springbootpay.wxPay.dto.NativePayParam;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-19 16:03
 * @description:
 **/
public interface IOrderService {
    /**
     * 创建微信支付订单
     * @return
     */
    String createWXOrder(NativePayParam reqData) throws Exception;
}
