package com.software.springbootpay.wxPay.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-19 15:45
 * @description: 订单金额
 **/
@Data
@Accessors(chain = true)
public class Amount {

    // 订单金额
    private int total;
    // 货币
    private String currency;


}