package com.software.springbootpay.wxPay.dto;

import lombok.Data;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-19 15:51
 * @description:
 **/
@Data
public class Payer {
    private String openid;
}