package com.software.logback.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-30 15:16
 * @description:
 **/
@Slf4j
@RestController
@RequestMapping("/logback")
public class LogbackController {

    @GetMapping("/test")
    public String test(){
        log.error("test error=====================》{}",System.currentTimeMillis());
        log.info("test=====================》{}",System.currentTimeMillis());
        return "test";
    }
}
