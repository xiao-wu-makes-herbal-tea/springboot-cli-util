package com.software.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootThymeleafApplication.class, args);
        System.out.println("thymeleaf: "+"http://localhost:9999/thymeleaf/getString");
        System.out.println("thymeleaf: "+"http://localhost:9999/thymeleaf/getStudents");
    }

}
