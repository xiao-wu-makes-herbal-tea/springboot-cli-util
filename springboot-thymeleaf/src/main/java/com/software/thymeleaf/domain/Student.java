package com.software.thymeleaf.domain;

import java.io.Serializable;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-18 16:03
 * @description:
 **/
public class Student  implements Serializable {
    private static final long serialVersionUID = -91969758749726312L;
    /**
     * 唯一标识id
     */
    private Integer id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}