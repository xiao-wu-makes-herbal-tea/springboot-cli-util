package com.software.thymeleaf.controller;

import com.software.thymeleaf.domain.Student;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;


/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-18 14:12
 * @description:
 **/
@Controller
@RequestMapping("/thymeleaf")
public class ThymeleafController {
    @RequestMapping("/getString")
    public String getString(HttpServletRequest request){
        String name = "thymeleaf demo";
        request.setAttribute("name",name);
        return "index";
    }
    @GetMapping("/getStudents")
    public ModelAndView getStudent(){
        List<Student> students = new LinkedList<>();
        Student student = new Student();
        student.setId(1);
        student.setName("张三");
        student.setAge(21);
        Student student1 = new Student();
        student1.setId(2);
        student1.setName("李四");
        student1.setAge(22);
        students.add(student);
        students.add(student1);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("students",students);
        modelAndView.setViewName("students");
        return modelAndView;
    }
}
