package com.software.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-18 14:12
 * @description:
 **/
@RestController
@RequestMapping("/fileConversion")
public class FileConversionController {
    @RequestMapping("/fileConversion")
    public String fileConversion(){
        return "File Conversion Service!";
    }
}
