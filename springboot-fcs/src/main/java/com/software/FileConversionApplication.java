package com.software;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-27 14:56
 * @description:
 **/
@SpringBootConfiguration
public class FileConversionApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileConversionApplication.class, args);
    }
}
