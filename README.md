## SpringBoot 工具脚手架

[![GitHub issues](https://img.shields.io/github/issues/yidao620c/SpringBootBucket.svg)](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/issues)
[![Github downloads](https://img.shields.io/github/downloads/yidao620c/SpringBootBucket/total.svg)](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/releases/latest)
[![GitHub release](https://img.shields.io/github/release/yidao620c/SpringBootBucket.svg)](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/releases)

1. Spring Boot 现在已经成为Java 开发领域的一颗璀璨明珠，它本身是包容万象的，可以跟各种技术集成。

   本项目对目前Web开发中常用的各个技术，通过和SpringBoot的集成，并且对各种技术通过“一篇博客 + 一个可运行项目”的形式来详细说明。

   每个子项目都会使用最小依赖，大家拿来即可使用，自己可以根据业务需求自由组合搭配不同的技术构建项目。

## 项目简介



#### 子项目列表

| 项目名称                                                     | 内容介绍                                  | 完成进度 |
| ------------------------------------------------------------ | :---------------------------------------- | -------- |
| [springboot-helloworld](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-helloworld) | springboot第一步                          | 100%     |
| [springboot-logback](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-logback) | springboot整合logback实现日志记录         | 100%     |
| [springboot-exception](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-exception) | springboot处理exception异常捕获           | 100%     |
| [springboot-aop](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-aop) | springboot使用AOP实现切面操作             | 100%     |
| [springboot-thymeleaf](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-thymeleaf) | springboot整合Thymeleaf构建Web应用        | 100%     |
| [springboot-redis](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-redis) | springboot整合Redis数据库实现缓存         | 100%     |
| [springboot-mybatis](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-mybatis) | springboot整合mybatis实现数据库操作       | 100%     |
| [springboot-mybatisPlus](https://gitee.com/xiao-wu-makes-herbal-tea/springboot-cli-util/tree/master/springboot-mybatisPlus) | springboot整合mybatisPlus实现对数据库操作 | 100%     |
| springboot-mongodb                                           | 集成MongoDB                               |          |
| springboot-transaction                                       | 声明式事务                                |          |
| springboot-shiro                                             | 集成Shiro权限管理                         |          |
| springboot-swagger2                                          | 集成Swagger2自动生成API文档               |          |
| springboot-jwt                                               | 集成JWT实现接口权限认证                   |          |
| springboot-multisource                                       | 多数据源配置                              |          |
| springboot-schedule                                          | 定时任务                                  |          |
| springboot-cxf                                               | cxf实现WebService                         |          |
| springboot-websocket                                         | 使用WebScoket实时通信                     |          |
| springboot-socketio                                          | 集成SocketIO实时通信                      |          |
| springboot-async                                             | 异步线程池                                |          |
| springboot-cache                                             | 使用缓存                                  |          |
| springboot-batch                                             | 批处理                                    |          |
| springboot-rabbitmq                                          | 使用消息队列RabbitMQ                      |          |
| springboot-echarts                                           | 集成Echarts导出图片                       |          |



## 环境

* JDK 1.8
* Maven latest
* Spring Boot 2.0.4
* Intellij IDEA
* mysql 5.7
* mongodb
* git 版本管理
* nginx 反向代理
* redis 缓存
* rabbitmq 消息队列

1.  https://gitee.com/gitee-stars/)
