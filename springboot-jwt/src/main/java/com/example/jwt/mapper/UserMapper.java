package com.example.jwt.mapper;

import com.example.jwt.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-16 13:50
 * @description:
 **/
@Mapper
@Repository
public interface UserMapper {
    User login(User user);
}
