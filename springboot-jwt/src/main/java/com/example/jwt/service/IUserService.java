package com.example.jwt.service;

import com.example.jwt.entity.User;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-16 13:53
 * @description:
 **/
public interface IUserService {
    User login(User user);
}
