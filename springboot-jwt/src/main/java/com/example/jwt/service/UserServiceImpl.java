package com.example.jwt.service;

import com.example.jwt.entity.User;
import com.example.jwt.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-16 13:53
 * @description:
 **/

@Service
@Transactional
public class UserServiceImpl implements IUserService{

    @Autowired
    private UserMapper userMapper;
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public User login(User user) {
        // 根据接收用户名密码查询数据库
        User userDB = userMapper.login(user);
        if (userDB!=null){
            return userDB;
        }
        throw  new RuntimeException("登录失败 -.-");
    }
}

