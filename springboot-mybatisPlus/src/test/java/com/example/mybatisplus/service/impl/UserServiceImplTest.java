package com.example.mybatisplus.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.example.domain.po.User;
import com.example.domain.query.UserParam;
import com.example.mybatisplus.pojo.PageResult;
import com.example.service.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-29 10:03
 * @description:
 **/
@SpringBootTest
public class UserServiceImplTest {
    @Resource
    private IUserService userService;
    @Test
    void testSaveBatch() {
        // 准备10万条数据
        List<User> list = new ArrayList<>(1000);
        long b = System.currentTimeMillis();
        for (int i = 1; i <= 100000; i++) {
            list.add(buildUser(i));
            // 每1000条批量插入一次
            if (i % 1000 == 0) {
                userService.saveBatch(list);
                list.clear();
            }
        }
        long e = System.currentTimeMillis();
        System.out.println("耗时：" + (e - b)+" ms");//耗时：33.488  s
        // 数据库配置信息加入&rewriteBatchedStatements=true后    耗时：10.529 s

    }
    @Test
    void testSaveOneByOne() {
        long b = System.currentTimeMillis();
        for (int i = 1; i <= 100000; i++) {
            userService.save(buildUser(i));
        }
        long e = System.currentTimeMillis();
        System.out.println("耗时：" + (e - b)+" ms");//耗时：292.468 s
    }

    private User buildUser(int i) {
        User user = new User();
        user.setUsername("user_" + i);
        user.setPassword("123");
        user.setPhone("" + (18688190000L + i));
        user.setBalance(2000);
        user.setInfo("{\"age\": 24, \"intro\": \"英文老师\", \"gender\": \"female\"}");
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(user.getCreateTime());
        return user;
    }
    @Test
    void testDelete() {
        long b = System.currentTimeMillis();
        userService.remove(new QueryWrapper<User>().like("username", "user"));
        long e = System.currentTimeMillis();
        System.out.println("耗时：" + (e - b)+" ms");//耗时：2.408 s
    }
    @Test
    void testWrapper() {
        User user = new User();
//        user.setId(5L);
//        user.setUsername("Lucy");
//        user.setPassword("123");
//        user.setPhone("18688990011");
        user.setBalance(200);
//        user.setInfo("{\"age\": 24, \"intro\": \"英文老师\", \"gender\": \"female\"}");
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        userService.update(user,new UpdateWrapper<User>().setSql("phone=18688990011"));
        userService.lambdaUpdate().setSql("phone=18688990011").update(user);

    }
    @Test
    void testDbGet() {
        User user = Db.getById(1L, User.class);
        System.out.println(user);
    }
    @Test
    void testDbSelect() {
//        Page<User> page = new Page<>(1, 2);
        UserParam userParam = new UserParam();
        IPage<User> userPage = userService.selectPage(userParam);

        userPage.getRecords().forEach(System.out::println);
    }
    @Test
    void testSelectPage() {
        UserParam param = new UserParam();

        PageResult<User> userPageResult = userService.selectResultPage(param.getMpPageByCreateTime(), new QueryWrapper<User>()
                .eq(false, "balance", 2000));
        System.out.println(userPageResult.getCurrent());
        System.out.println(userPageResult.getSize());
        System.out.println(userPageResult.getTotal());
        System.out.println(userPageResult.getList());

    }
}