package com.example.mybatisplus.mapper;

import com.example.domain.po.User;
import com.example.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-29 11:05
 * @description:
 **/
@SpringBootTest
class UserMapperTest {

    @Resource
    private UserMapper userMapper;


    @Test
    void testInsert() {
        User user = new User();
        user.setId(5L);
        user.setUsername("Lucy");
        user.setPassword("123");
        user.setPhone("18688990011");
        user.setBalance(200);
        user.setInfo("{\"age\": 24, \"intro\": \"英文老师\", \"gender\": \"female\"}");
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        System.out.println("userMapper.insert(user) = " + userMapper.insert(user));
    }

    @Test
    void testSelectById() {
        User user = userMapper.selectById(5L);
        System.out.println("user = " + user);
    }

    @Test
    void testSelectByIds() {
        ArrayList<Long> userIds = new ArrayList<>();
        userIds.add(1L);
        userIds.add(2L);
        userIds.add(3L);
        userIds.add(4L);
        userIds.add(5L);
        List<User> users = userMapper.selectBatchIds(userIds);
        users.forEach(System.out::println);
    }

    @Test
    void testUpdateById() {
        User user = new User();
        user.setId(5L);
        user.setBalance(20000);
        System.out.println("userMapper.updateById(user) = " + userMapper.updateById(user));
    }

    @Test
    void testDelete() {
        System.out.println("userMapper.deleteById(5L) = " + userMapper.deleteById(5L));
    }
}