package com.example.service;

import com.example.domain.po.User;
import com.example.mybatisplus.plus.IServiceX;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-19 11:49
 * @description:
 **/
public interface IUserService extends IServiceX<User> {

    void deductBalance(Long id, Integer money);
}
