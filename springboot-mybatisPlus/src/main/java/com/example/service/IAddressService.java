package com.example.service;

import com.example.domain.po.Address;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Wu ZiLin
 * @since 2023-11-29
 */
public interface IAddressService extends IService<Address> {

}
