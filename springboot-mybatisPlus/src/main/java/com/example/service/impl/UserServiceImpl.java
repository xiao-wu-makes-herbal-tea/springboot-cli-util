package com.example.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.domain.po.User;
import com.example.mybatisplus.pojo.PageParam;
import com.example.service.IUserService;
import com.example.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-19 11:49
 * @description:
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements IUserService {


    @Override
    public void deductBalance(Long id, Integer money) {
        PageParam param = new PageParam();
        this.page(param.getMpPage());
        User user = getById(id);
        if (user==null||user.getStatus()==2){
            throw new RuntimeException("用户不存在");
        }
        if (user.getBalance()<money){
            throw new RuntimeException("余额不足");
        }
        baseMapper.deductMoneyById(id, money);
    }
}




