package com.example.service.impl;

import com.example.domain.po.Address;
import com.example.mapper.AddressMapper;
import com.example.service.IAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wu ZiLin
 * @since 2023-11-29
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements IAddressService {

}
