package com.example.mapper;

import com.example.domain.po.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-19 11:49
 * @description:
 **/
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Update("update user set balance = balance - #{money} where id = #{id}")
    void deductMoneyById(Long id, Integer money);
}




