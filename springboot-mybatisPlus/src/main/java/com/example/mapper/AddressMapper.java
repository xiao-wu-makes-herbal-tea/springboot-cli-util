package com.example.mapper;

import com.example.domain.po.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wu ZiLin
 * @since 2023-11-29
 */
@Mapper
public interface AddressMapper extends BaseMapper<Address> {

}
