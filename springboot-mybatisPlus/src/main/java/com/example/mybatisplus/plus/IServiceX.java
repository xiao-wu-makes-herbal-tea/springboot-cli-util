package com.example.mybatisplus.plus;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mybatisplus.pojo.PageParam;
import com.example.mybatisplus.pojo.PageResult;
import org.apache.ibatis.annotations.Param;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-29 17:16
 * @description:
 **/
public interface IServiceX<T> extends IService<T> {
    /**
     * 默认分页 1/10
     *
     * @return Page
     */
    default Page<T> selectDefaultPage() {
        return page(new Page<>(1, 10));
    }

    /**
     * 自定义param分页
     *
     * @return Page
     */
    default Page<T> selectPage(PageParam param) {
        Page<T> mpPage = new Page<>(param.getPageNo(), param.getPageSize());
        return page(mpPage);
    }

    /**
     * 自定义param分页
     *
     * @return Page
     */
    default Page<T> selectPage(PageParam param, @Param("ew") Wrapper<T> wrapper) {
        Page<T> mpPage = new Page<>(param.getPageNo(), param.getPageSize());
        return page(mpPage, wrapper);
    }

    /**
     * PageResult默认分页 1/10
     *
     * @return PageResult
     */
    default PageResult<T> selectDefaultResultPage() {
        return selectResultPage(new Page<>(1, 10), Wrappers.emptyWrapper());
    }

    /**
     * 自定义param分页返回PageResult
     *
     * @return PageResult
     */
    default PageResult<T> selectResultPage(PageParam param) {
        return selectResultPage(param, Wrappers.emptyWrapper());
    }

    /**
     * 自定义param分页返回PageResult
     *
     * @return PageResult
     */
    default PageResult<T> selectResultPage(PageParam param, @Param("ew") Wrapper<T> wrapper) {
        // MyBatis Plus 查询
        IPage<T> mpPage = new Page<>(param.getPageNo(), param.getPageSize());
        return selectResultPage(mpPage, wrapper);
    }

    /**
     * 分页返回PageResult
     *
     * @return PageResult
     */
    default PageResult<T> selectResultPage(IPage<T> page) {
        return selectResultPage(page, Wrappers.emptyWrapper());
    }

    /**
     * 分页返回PageResult
     *
     * @return PageResult
     */
    default PageResult<T> selectResultPage(IPage<T> page, @Param("ew") Wrapper<T> wrapper) {
        // MyBatis Plus 查询
        page(page, wrapper);
        // 转换返回
        return new PageResult<>(page.getCurrent(), page.getSize(), page.getRecords(), page.getTotal());
    }

}
