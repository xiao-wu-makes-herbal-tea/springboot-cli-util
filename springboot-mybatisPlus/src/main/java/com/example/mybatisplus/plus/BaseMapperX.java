package com.example.mybatisplus.plus;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mybatisplus.pojo.PageParam;
import com.example.mybatisplus.pojo.PageResult;
import org.apache.ibatis.annotations.Param;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-28 18:28
 * @description: 在 MyBatis Plus 的 BaseMapper 基础上拓展，提供更多的能力
 **/
public interface BaseMapperX<T> extends BaseMapper<T> {
    /**
     * 默认分页 1/10
     *
     * @return Page
     */
    default Page<T> selectDefaultPage() {
        return selectPage(new Page<>(1, 10), Wrappers.emptyWrapper());
    }

    /**
     * PageResult默认分页 1/10
     * @return PageResult
     */
    default PageResult<T> selectDefaultResultPage() {
        return selectResultPage(new Page<>(1, 10), Wrappers.emptyWrapper());
    }

    default PageResult<T> selectResultPage(PageParam param) {
        return selectResultPage(param, Wrappers.emptyWrapper());
    }

    default PageResult<T> selectResultPage(PageParam param, @Param("ew") Wrapper<T> wrapper) {
        // MyBatis Plus 查询
        IPage<T> mpPage = new Page<>(param.getPageNo(), param.getPageSize());
        return selectResultPage(mpPage, wrapper);
    }

    default PageResult<T> selectResultPage(IPage<T> page) {
        return selectResultPage(page, Wrappers.emptyWrapper());
    }

    default PageResult<T> selectResultPage(IPage<T> page, @Param("ew") Wrapper<T> wrapper) {
        // MyBatis Plus 查询
        selectPage(page, wrapper);
        // 转换返回
        return new PageResult<>(page.getCurrent(), page.getSize(), page.getRecords(), page.getTotal());
    }

}
