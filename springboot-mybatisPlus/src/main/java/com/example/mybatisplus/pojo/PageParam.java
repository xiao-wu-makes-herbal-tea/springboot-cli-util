package com.example.mybatisplus.pojo;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-29 18:10
 * @description:
 **/
@Data
public class PageParam {
    private static int PAGE_NO = 1;
    private static int PAGE_SIZE = 10;

    private Integer pageNo = PAGE_NO;
    private Integer pageSize = PAGE_SIZE;

    /**
     * @param <T>
     * @return MP分页
     */
    public <T> Page<T> getMpPage() {
        return new Page<T>(pageNo, pageSize);
    }

    /**
     * @param sortBy 排序
     * @param isAsc  是否正序
     * @param <T>
     * @return MP分页指定字段排序
     */
    public <T> Page<T> getMpPage(String sortBy, Boolean isAsc) {
        return getMpPage(new OrderItem(sortBy, isAsc));
    }

    /**
     * 根据创建时间分页
     *
     * @param <T>
     * @return MP分页通过创建时间升序
     */
    public <T> Page<T> getMpPageByCreateTime() {
        return getMpPageByCreateTime(true);
    }

    /**
     * 根据更新时间分页
     *
     * @param <T>
     * @return MP分页通过更新时间升序
     */
    public <T> Page<T> getMpPageByUpdateTime() {
        return getMpPageByUpdateTime(true);
    }

    /**
     * 根据创建时间分页
     *
     * @param isAsc
     * @param <T>
     * @return MP分页通过创建时间排序
     */
    public <T> Page<T> getMpPageByCreateTime(Boolean isAsc) {
        return getMpPage(new OrderItem("create_time", isAsc));
    }

    /**
     * 根据更新时间分页
     *
     * @param isAsc
     * @param <T>
     * @return MP分页通过更新时间排序
     */
    public <T> Page<T> getMpPageByUpdateTime(Boolean isAsc) {
        return getMpPage(new OrderItem("update_time", isAsc));
    }


    /**
     * @param orderItem 排序字段
     * @param <T>
     * @return MP分页
     */
    public <T> Page<T> getMpPage(OrderItem... orderItem) {
        Page<T> page = Page.of(pageNo, pageSize);
        if (orderItem != null) {
            page.addOrder(orderItem);
        }
        return page;
    }

}
