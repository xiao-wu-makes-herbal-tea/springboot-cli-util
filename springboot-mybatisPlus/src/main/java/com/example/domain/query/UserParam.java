package com.example.domain.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.domain.po.User;
import com.example.mybatisplus.pojo.PageParam;
import lombok.Data;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-11-29 18:13
 * @description:
 **/
@Data
public class UserParam extends PageParam {
    private String name;
}
