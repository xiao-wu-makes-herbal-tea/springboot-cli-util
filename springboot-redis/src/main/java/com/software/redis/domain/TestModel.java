package com.software.redis.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2023-10-31 09:30
 * @description:
 **/
@Data
public class TestModel implements Serializable {
    /**
     * @Fields serialVersionUID : (用一句话描述这个变量表示什么)
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
}