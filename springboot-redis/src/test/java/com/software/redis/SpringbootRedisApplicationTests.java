package com.software.redis;

import com.software.redis.domain.TestModel;
import com.software.redis.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SpringBootTest
class SpringbootRedisApplicationTests {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Test
    void contextLoads() {
        // 测试对象
        TestModel testModel = new TestModel();
        testModel.setId(System.currentTimeMillis());
        testModel.setName("测试");
        redisTemplate.opsForValue().set("testModel::11", testModel);
        Object testModel1 = redisTemplate.opsForValue().get("testModel::11");
        System.out.println("testModel1 = " + testModel1);
        TestModel testModel2 = (TestModel) redisTemplate.opsForValue().get("testModel::11");
        System.err.println(testModel2);
        System.err.println(testModel2.getName());
        System.err.println(testModel2.getId());

    }
    @Test
    void contextLoads2() {
        // 测试对象1
        TestModel testModel1 = new TestModel();
        testModel1.setId(System.currentTimeMillis());
        testModel1.setName("测试1");
        // 测试对象2
        TestModel testModel2 = new TestModel();
        testModel2.setId(System.currentTimeMillis()+1L);
        testModel2.setName("测试2");
        List<TestModel> testModelList = new ArrayList<>();
        testModelList.add(testModel1);
        testModelList.add(testModel2);
        System.out.println("testModelList = " + testModelList);
        redisTemplate.opsForValue().set("testModel::list", testModelList);
        Object testModelListO = redisTemplate.opsForValue().get("testModel::list");
        System.err.println("testModelListO = " + testModelListO);
        List<TestModel> testModelList2 =(List<TestModel>) redisTemplate.opsForValue().get("testModel::list");
        System.err.println("testModelList2 = " +testModelList2);
    }
    @Test
    void contextLoadsList() {
        // 测试对象1
        TestModel testModel1 = new TestModel();
        testModel1.setId(System.currentTimeMillis());
        testModel1.setName("测试1");
        // 测试对象2
        TestModel testModel2 = new TestModel();
        testModel2.setId(System.currentTimeMillis()+1L);
        testModel2.setName("测试2");
        List<TestModel> testModelList = new ArrayList<>();
        testModelList.add(testModel1);
        testModelList.add(testModel2);
        System.out.println("testModelList = " + testModelList);
        redisTemplate.delete("testModel::list1");
        for (TestModel testModel : testModelList) {
            redisTemplate.opsForList().rightPushAll("testModel::list1", testModel);
        }
        Object testModelListO = redisTemplate.opsForList().range("testModel::list1",0,-1);
        System.err.println("testModelListO = " + testModelListO);

       String range = redisTemplate.opsForList().range("testModel::list1", 0, -1).toString();
        System.err.println("range = " + range);
//        System.err.println("testModelList2 = " +testModelList2);
    }
}
