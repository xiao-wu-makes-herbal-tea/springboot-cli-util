package com.example.springdatajpademo.dao;

import com.example.springdatajpademo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2024-01-25 17:08
 * @description:
 **/
@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
}
