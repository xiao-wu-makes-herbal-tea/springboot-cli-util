package com.example.springdatajpademo.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;



/**
 * @program: springboot-cli-util
 * @author: Wu ZiLin
 * @create: 2024-01-25 17:09
 * @description:
 **/
@SpringBootTest
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void testQuery(){
        userRepository.findById(1).ifPresent(System.out::println);
    }

}

